namespace NodeJS {
  interface ProcessEnv extends NodeJS.ProcessEnv {
    MONGODB_URI: string
  }

  interface Global extends NodeJS.Global {
    mongoose: any
  }
}

interface IRequestBloodProps {
  BloodGroup: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
  RequestingFor: string
  BloodDonationType: 1 | 2 | 3
}

interface IResponseBloodProps {
  RequestId: string
}
