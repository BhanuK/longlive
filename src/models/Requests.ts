import mongoose from 'mongoose'

/* RequestSchema will correspond to a collection in your MongoDB database. */
const RequestSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please provide a name for this request.'],
    maxlength: [20, 'Name cannot be more than 60 characters']
  },
  owner_name: {
    type: String,
    required: [true, "Please provide the request owner's name"],
    maxlength: [20, "Owner's Name cannot be more than 60 characters"]
  },

  age: {
    type: Number
  },
  is_cancelled: {
    type: Boolean
  },
  mobile_numbers: {
    type: Array
  },
  image_url: {
    required: [true, 'Please provide an image url for this request.'],
    type: String
  },
  interested_queue: {
    type: Array
  }
})

export default mongoose.models.Request || mongoose.model('Request', RequestSchema)
