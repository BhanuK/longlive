import Head from 'next/head'

import { Hero } from 'components/Hero'
import { Main } from 'components/Main'

import db from '../utils/db'
import Requests from 'models/Requests'

const Index = () => (
  <>
    <Head>
      <title>Live Long</title>
    </Head>

    <Hero />
    <Main>Hi</Main>
  </>
)

export async function getServerSideProps() {
  await db()

  /* find all the data in our database */
  const result = await Requests.find({})
  const requests = result.map(doc => {
    const request = doc.toObject()
    request._id = request._id.toString()
    return request
  })

  return { props: { requests: {} } }
}

export default Index
