import Head from 'next/head'
import { FormControl, FormLabel, FormHelperText, FormErrorMessage, Select, useToast, Heading, Button } from '@chakra-ui/react'
import { useState } from 'react'
import { useForm } from 'react-hook-form'

const Requests = () => {
  const toast = useToast()
  const [bloodRequest, setBloodRequest] = useState({ requestId: '' })

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<IRequestBloodProps>()

  const onSubmit = async (data: IRequestBloodProps): Promise<void> => {
    // const response = await Services.RequestBlood({ RequestingFor: data.RequestingFor, BloodGroup: data.BloodGroup, BloodDonationType: data.BloodDonationType })

    // setBloodRequest({
    //     requestId: response?.RequestId!,
    // })

    toast({
      title: 'Request created.',
      description: `We've created your request for blood`,
      position: 'top-right',
      status: 'success',
      duration: 9000,
      isClosable: true
    })
  }

  return (
    <>
      <Head>
        <title>Requests - Live Long</title>
      </Head>
      <Heading marginBottom={'2rem'}>Requests</Heading>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl isInvalid={errors.RequestingFor !== undefined} pb="2">
          <FormLabel htmlFor="RequestingFor">Requesting For</FormLabel>
          <Select
            name="RequestingFor"
            {...register('RequestingFor', { required: 'Please select to whom you are requesting for' })}
            borderColor="blue.300"
            borderWidth="1px"
          >
            <option value="">-SELECT-</option>
            <option value="Self">Self</option>
            <option value="Friend">Friend</option>
            <option value="Relative">Relative</option>
            <option value="Other"> Other</option>
          </Select>
          <FormHelperText>Relation with the person.</FormHelperText>
          <FormErrorMessage>{errors.RequestingFor && errors.RequestingFor.message}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.BloodGroup !== undefined} pb="2">
          <FormLabel htmlFor="BloodGroup">Blood Group</FormLabel>
          <Select name="BloodGroup" {...register('BloodGroup', { required: 'Blood group is required' })} borderColor="blue.300" borderWidth="1px">
            <option value="">-SELECT-</option>
            <option value="1">A +ve</option>
            <option value="2">A -ve</option>
            <option value="3">B +ve</option>
            <option value="4">B -ve</option>
            <option value="5">O +ve</option>
            <option value="6">O -ve</option>
            <option value="7">AB +ve</option>
            <option value="8">AB -ve</option>
          </Select>
          <FormHelperText>Blood group of the person.</FormHelperText>
          <FormErrorMessage>{errors.BloodGroup && errors.BloodGroup.message}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.BloodDonationType !== undefined} pb="2">
          <FormLabel htmlFor="BloodDonationType">Donation Type</FormLabel>
          <Select
            name="BloodDonationType"
            {...register('BloodDonationType', { required: 'Blood donation type is required' })}
            borderColor="blue.300"
            borderWidth="1px"
          >
            <option value="">-SELECT-</option>
            <option value="1">Whole Blood Donation</option>
            <option value="2">Plasma Donation (Apheresis)</option>
            <option value="3">Platelet Donation (Plateletpheresis)</option>
          </Select>
          <FormHelperText>Blood Donation Type.</FormHelperText>
          <FormErrorMessage>{errors.BloodDonationType && errors.BloodDonationType.message}</FormErrorMessage>
        </FormControl>
        <Button colorScheme="blue" type="submit" w="full" mt="4">
          Submit
        </Button>
      </form>
    </>
  )
}

export default Requests
