import {
  chakra,
  Box,
  Flex,
  useColorModeValue,
  VisuallyHidden,
  Link,
  HStack,
  Button,
  useDisclosure,
  VStack,
  IconButton,
  CloseButton,
  Text
} from '@chakra-ui/react'
import { default as NextLink } from 'next/link'
import { AiOutlineMenu } from 'react-icons/ai'
import { DarkModeSwitch } from 'components/DarkModeSwitch'
import { Footer } from 'components/Footer'
import { Container } from 'components/Container'

const Layout = ({ children }) => {
  const bg = useColorModeValue('white', 'gray.800')
  const mobileNav = useDisclosure()
  // const { user, userLoggedIn } = useAuthContext()

  return (
    <>
      <chakra.header bg={bg} w="full" px={{ base: 2, sm: 4 }} py={3} shadow="md">
        <Flex align="center" justify="space-between" mx="auto">
          <Flex>
            <NextLink href="/">
              <chakra.h1 fontSize="2xl" fontWeight="bold" px={3} colorScheme="brand">
                Long Live
              </chakra.h1>
            </NextLink>
          </Flex>
          <HStack display="flex" align="center" spacing={1}>
            <HStack spacing={1} mr={1} color="brand.500" display={{ base: 'none', md: 'inline-flex' }}>
              <NextLink href="/">
                <Button variant="ghost">Home</Button>
              </NextLink>
              <NextLink href="/requests">
                <Button variant="ghost">Requests</Button>
              </NextLink>

              {/* {userLoggedIn ? (
              <>
                <Button variant="ghost" to="/profile">
                  {user?.displayName || 'Profile'}
                </Button>
                <Button variant="ghost" colorScheme="red" to={Urls.SIGN_OUT}>
                  Sign Out
                </Button>
              </>
            ) : (
              <Button variant="ghost" to="/sign-in">
                Sign In
              </Button>
            )} */}
            </HStack>
            {/* {userLoggedIn ? null : (
            <Button colorScheme="brand" size="sm" p={5}>
              Sign Up
            </Button>
          )} */}
            <Box display={{ base: 'inline-flex', md: 'none' }}>
              <IconButton
                display={{ base: 'flex', md: 'none' }}
                aria-label="Open menu"
                fontSize="20px"
                color={useColorModeValue('gray.800', 'inherit')}
                variant="ghost"
                icon={<AiOutlineMenu />}
                onClick={mobileNav.onOpen}
              />

              <VStack
                pos="absolute"
                top={0}
                left={0}
                right={0}
                display={mobileNav.isOpen ? 'flex' : 'none'}
                flexDirection="column"
                p={2}
                pb={4}
                m={2}
                bg={bg}
                spacing={3}
                rounded="sm"
                shadow="sm"
              >
                <CloseButton aria-label="Close menu" onClick={mobileNav.onClose} />
                <NextLink href="/">
                  <Button w="full" variant="ghost">
                    Home
                  </Button>
                </NextLink>
                <NextLink href="/requests">
                  <Button w="full" variant="ghost">
                    Requests
                  </Button>
                </NextLink>

                {/* {userLoggedIn ? (
                <>
                  <Link to="/profile">{user?.displayName || 'Profile'}</Link>
                  <Button w="full" variant="ghost">
                    Sign Out
                  </Button>
                </>
              ) : (
                <Button w="full" variant="ghost">
                  Sign In
                </Button>
              )} */}
              </VStack>
            </Box>
          </HStack>
        </Flex>
      </chakra.header>
      <Container>
        <Box as="main" padding={2} height="60vh">
          {children}
        </Box>

        <Footer>
          <Text>Digital Ocean ❤️ Mongo</Text>
        </Footer>
      </Container>
      <DarkModeSwitch />
    </>
  )
}

export default Layout
